import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy.stats import entropy

class Decisiontree_functions:
    """
    ノードクラスや決定木クラスで使用する関数をまとめる。
    """
    def infomation_gain(self,parent_impurity,Np, derive_impurity1,Ns1,derive_impurity2,Ns2):
        gain = parent_impurity - (Ns1/Np)*derive_impurity1 - (Ns2/Np)*derive_impurity2
        return gain

    def numbering(self,pre_number,class_number):
        if class_number == 0:
            return bin(int(pre_number, 0) * int(bin(2), 0))
        if class_number == 1:
            return bin(int(pre_number, 0) * int(bin(2), 0) + int(bin(1), 0))





class Node:
    """
    決定木アルゴリズムはデータの集合（ノード）を条件によって枝分かれさせることで予測を立てる。
    そのため、ノードオブジェクトを作成し、枝分かれさせることで予測する予測器を作成する。
    """

    def __init__(self, matrix):  # 一番右の列がｙのnumpy.array
        self.matrix = matrix
        self.row = matrix.shape[0]
        self.line = matrix.shape[1]
        self.y = self.matrix[:, -1]
        classes = [0, 1]
        samples = []
        for yclass in classes:
            samples.append(self.y[self.y == yclass].shape[0])
        self.samples = samples
        self.cross_entropy = entropy(samples, base=2)



    def gini_impurity(self):
        Ig = 1
        for sample in self.samples:
            Ig -= (sample / self.row) ** 2
        return Ig

    def branch(self):  # 情報利得が最大となるように枝分かれ
        func = Decisiontree_functions()
        gain_max = []
        index = []
        for column in range(self.line - 1):
            data = self.matrix[np.argsort(self.matrix[:, column])]
            gains = []
            for i in range(1, self.matrix.shape[0]):
                node1 = Node(data[:i])
                node2 = Node(data[i:])
                gains.append(func.infomation_gain(self.cross_entropy,self.row,node1.cross_entropy,node1.row,node2.cross_entropy,node2.row))
            gains = np.array(gains)
            gain_max.append(gains.max())
            index.append(gains.argmax())
        gains_max = np.array(gain_max)
        index = np.array(index)
        data = self.matrix[np.argsort(self.matrix[:, gains_max.argmax()])]
        node1 = Node(data[:index.max()])
        node2 = Node(data[index.max():])
        return node1, node2

    def brancher(self,node_number):
        func = Decisiontree_functions()
        node1, node2 = self.branch()
        new_dic = {func.numbering(node_number, 0): node1, func.numbering(node_number, 1): node2}

class DecisionTree:
    def __init__(self):
        pass


    def fit(self, test_x, test_y):
        func = Decisiontree_functions()
        df = pd.concat([test_x, test_y])
        data = df.values
        node = Node(data)
        #--------------------------------
        node_dict = {0b1:node}
        node1, node2 = node.branch()
        new_dic = {func.numbering(0b1,0):node1,func.numbering(0b1,1):node2}
        node_dict.append(new_dic)
        #-----------------------------------


    def predict(self):
        pass

    def acscore(self):
        pass

    def descride(self):
        pass

    def plot(self):
        pass
